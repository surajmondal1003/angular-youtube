import { Component, OnInit } from '@angular/core';
import { VideosSuggestionService } from './video-suggestion.service';



@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  searchTerm:string='';
 
  videoSuggestionList:any[];
  videoList:any[];
  constructor(private suggestionService:VideosSuggestionService) { }

  ngOnInit() {
    // this.videoList=this.suggestionService.getVideos('space');
    // console.log(this.videoList);
  }

  getVideoSuggestion(){
    this.videoSuggestionList=this.suggestionService.getVideoSuggestion(this.searchTerm);
    //console.log(this.videoSuggestionList);
    
  }

  onSubmit(searchkey:string){
    
    this.searchTerm=searchkey;
    this.videoList=this.suggestionService.getVideos(this.searchTerm);

    // const imageUrl = videos.snippet.thumbnails.default.url;
    // const title= video.snippet.title;
    console.log(this.videoList);
    this.videoSuggestionList=[];
    
  }

 
 

}
