import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import YTSearch from "youtube-api-search";


@Injectable()
export class VideosSuggestionService{
    API_KEY = "AIzaSyCoZQroz6O4BpS2C12_lVGOOqSA_Rv1OmY";

    videosSuggestions:any[];
    videoList:any[];
    constructor(private http:HttpClient){}

    getVideoSuggestion(searchkey:string){

        
        this.http.get("http://suggestqueries.google.com/complete/search?client=chrome&ds=yt&q="+searchkey).subscribe(
            (data : Response) => {
                this.videosSuggestions=data[1];
                
            }
        );
        return this.videosSuggestions;
    }

    getVideos(term:string){
        console.log('service-called');

        YTSearch({ key: this.API_KEY, term: term }, videos => {
           
            this.videoList=videos;
        });

        return this.videoList;
    }
}